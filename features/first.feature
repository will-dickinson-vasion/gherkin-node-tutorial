Feature: My feature

    Scenario: Session ID matches an admin user that is not logged in
        Given default instance url with path "/api/logged-in-admin/"
        When header
            | header | value     |
            | Cookie | sessionId |
        When method "get"
        Then response status code "401"
        Then the response contains '{"meta":{"isAdmin":false}}'